// console.log("awdawdwa")

// Array Methods - Javascript has built-in functions and methods for arrays. This allows us to manipulate and access array items.

// Mutator Methods
/*
	- Mutator methods are functions that "mutate" or change an array after they're created.

	- These methods manipulate the original array performing various task such as adding and removing elements.


*/


// push()
/*
	- Adds an element in the end of an array and returns the array's length


	Syntax:
		arrayName.push();


*/


let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

console.log("Current Array:");
console.log(fruits);

/*shows the added integer*/
let fruitsSample = fruits.push('Mango');

console.log(fruitsSample);
console.log("Mutated array from push method:");
console.log(fruits);


// Adding multiple elements to an array ****

fruits.push('Avocado', 'Guava');
console.log('Mutataed array from push method:')
;

console.log(fruits);

// pop();
/*
	Removes the last element in an array and returns the removed element.

	arrayName.pop('');



*/

/* showing the removed item */
let removedFruit = fruits.pop(); 
console.log(removedFruit);
console.log('Mutated array from pop method');
console.log(fruits);




// inshift()
/*
	adds one or more elements at the beginning of an array.

	Syntax:

		arrayName.unshift('elementA', 'elementB');


*/


fruits.unshift('Lime','Banana');
console.log("mutated array from unshift method");
console.log(fruits);

// Shift(); method

/*

	- removes an element at the beginning
		of an array and returns the removed element.


	Syntax:
		arrayName.shift();

*/

let fruitsSheft = fruits.shift();
console.log(fruitsSheft);
console.log("Palatandaan wasdis");
console.log(fruits);



// splice()
/*
	Simultaneously removes elements from specified index number and adds elements


	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);



*/



// fruits.splice(0, 3, 'Durian');
fruits.splice(1, 2);
console.log("Palatandaan splce");
console.log(fruits);




// *******************************

// sort()
/*
	Rearrange the array elements in alphanumeric order.

	Syntax:
		
		arrayName.sort();





*/
fruits.sort();
console.log("Palatandaan ------------");
console.log(fruits);


// reverse()
/*

	reverse the order of array elements

	Syntax:
	arrayName.reverse();

*/


fruits.reverse();
console.log("Palatandaan***************");
console.log(fruits);



// Non-Mutator methods
/*
	Non-Mutator methods are functions that not modify or change an array after they're created.

	these methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing output.


*/


// indexOf();
/*

	- returns the index number of the first matching element found in an array
	- if no match was found, the result will be negative 1.
	- the search process will be done from first element proceeding to the last element.


	Syntax:
		arrayName.indexOf(seacrhValue);


*/


let countries = ['USA', 'PH', 'CAN', 'SG', 'THI', 'BRU', 'FRA', 'IRA']

let firstIndex = countries.indexOf('CAN');
console.log('Result: ' + firstIndex);


// slice();

/*
	Portion/slices elements from an array and returns a new array.

	Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);




*/

// Slicing of elements from a specified index to the first element 

let slicedArray = countries.slice(3);
console.log("Result *********");
console.log(slicedArray);


// ********************* start from last


let slicedArrayC= countries.slice(-3);
console.log("Palatandaan ");
console.log(slicedArrayC);




// Slicing off elements from a specified index to another index
// Note: the last element is not included.

let sliceArrayB = countries.slice(0, 3);
console.log("Palatandaan &&&&&&&&&&&&&");
console.log(sliceArrayB);




// forEach();
/*
	Similar to  a for loop that iterates on each array element.

	for each item in the array, the anonymous function passed in forEach() method will run.

	Syntax:
		arrayName.forEach(function(indivelement){
			statement
		})
			



*/


 countries.forEach(function(country) {
 	console.log(country);
 });




 // includes();
/*

 	includes() method checks if the argument passed can be found in the array.

	it returns a boolen which can be saved in a variable.

	return true if the argument is found in the array.

	return false if it is not.

	Syntax:
		arrayName.includes(<argumentToFind>);

*/
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes("Mouse");

        console.log(productFound1);//returns true

        let productFound2 = products.includes("Headset");

        console.log(productFound2);//returns false
















