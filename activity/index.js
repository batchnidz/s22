/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
    
/*    function registeredUsers1(){
        let find = registeredUsers.includes('James Jeffries',  "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime");

        if(find == true){
            alert("Registration Failed! Username already exists!");
    }else{
        alert('Thank you');
    }
        }

        registeredUsers1();*/


        function register(username){
             let doesUserExist = registeredUsers.includes(username);

             if(doesUserExist){
                alert("Already registered")
             } else{
                registeredUsers.push(username)
                alert("Successfully registered")
             }
        }

      

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

    function addFriend(username){
/*
        "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
*/          
    


         if(registeredUsers.includes(username)){
        friendsList.push(username)
        alert(`You have added ${username} as a friend!`)
    } else {
        alert("User not found.")
    }
}



        /*if(friendsList == 'Akiko Yukihime' && friendsList == 'Gunther Smith'){
            alert("Already Friends")
        }else if (registeredUsers.indexOf('Akiko Yukihime') && friendsList.indexOf('Akiko Yukihime') != 'Akiko Yukihime'){
             friendsList.push('Akiko Yukihime')
             alert('Added Friend')
        }else if(registeredUsers.indexOf('Gunther Smith') && friendsList !== 'Gunther Smith'){
            friendsList.push('Gunther Smith')
            alert('Added Friend')
        }else{
            alert("User Not found")
        }
*/
  

        // addFriend();


/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
    
/*  function friendList(){
    if(friendsList <= 0){
        alert("You currently have 0 friends. Add one first.")
    }else{
        console.log("You currently have number of friends " + friendsList.length);
    }
  }


  friendList(); */
/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/


/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

    function displayFriends(){

            friendsList.forEach(function(friend) {
    console.log(friend);
 });
            if (friendsList.length === 0){
        alert("You currently have 0 friends. Add one first.")
    }

    }



    function displayNumbersFriends(){
    if (friendsList.length === 0){
        alert("You currently have 0 friends. Add one first.")
    } else {
        alert(`You currently have ${friendsList.length} friends.`)
    }
}



function deleteFriend(){
    friendsList.pop();
    if (friendsList.length === 0){
        alert("You currently have 0 friends. Add one first.")
    }
}



    function deleteSpecific(username){

        if(friendsList.includes(username)){
            friendsList.splice(username, 1)
            alert("User Deleted")
        }else{
            alert("user not found")
        }
    }

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/





